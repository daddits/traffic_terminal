<?php
require_once __DIR__ . '/vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as Capsule;

$capsule = new Capsule();

$capsule->addConnection([
    'driver' => 'mysql',
    'host' => 'localhost',
    'database' => 'traffic',
    'username' => 'root',
    'password' => ''
]);

$capsule->setAsGlobal();
$capsule->bootEloquent();

