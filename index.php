<?php
require __DIR__ . './vendor/autoload.php';

use Commands\EmployeeAll;
use Commands\EmployeeInfo;
use Commands\EmployeeCheckRole;

use Seeders\EmployeesSeeder;
use Seeders\RolesSeeder;
use Seeders\EmployeeRolesSeeder;

use Symfony\Component\Console\Application;

$application = new Application();

$application->add(new EmployeeAll());
$application->add(new EmployeeInfo());
$application->add(new EmployeeCheckRole());

$application->add(new EmployeesSeeder());
$application->add(new RolesSeeder());
$application->add(new EmployeeRolesSeeder());


$application->run();