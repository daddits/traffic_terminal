<?php
require_once '../config.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->dropIfExists('roles');

Capsule::schema()->create('roles', function (Blueprint $table) {
    $table->increments('id');
    $table->string('title', 20);
    $table->timestamps();
});