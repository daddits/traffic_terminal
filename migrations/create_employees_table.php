<?php
require_once '../config.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->dropIfExists('employees');

Capsule::schema()->create('employees', function (Blueprint $table) {
    $table->increments('id');
    $table->string('name', 20);
    $table->timestamps();
});