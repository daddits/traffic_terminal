<?php
require_once '../config.php';

use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Database\Schema\Blueprint;

Capsule::schema()->dropIfExists('employee_role');

Capsule::schema()->create('employee_role', function (Blueprint $table) {
    $table->increments('id');
    $table->integer('employee_id');
    $table->integer('role_id');
    $table->timestamps();
});
