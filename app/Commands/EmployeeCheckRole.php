<?php

namespace Commands;

use Models\Employee;
use Models\EmployeeRole;
use Models\Role;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class EmployeeCheckRole
 */
class EmployeeCheckRole extends Command
{
    /**
     * Console command for get info about employee's roles(configure)
     */
    protected function configure(): void
    {
        $this->setName('can: ')
            ->setDescription('Check role for employee')
            ->setHelp('Check role for employee')
            ->addArgument('employee', InputArgument::REQUIRED, 'title of employee')
            ->addArgument('role', InputArgument::REQUIRED, 'title of role');
    }

    /**
     * Console command for get info about employee's roles (execute)
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $employee = $input->getArgument('employee');
        $role = $input->getArgument('role');
        /**@var Employee* */
        $employeeResultSearch = Employee::where('name', $employee)->first();
        /**@var Role* */
        $roleResultSearch = Role::where('title', $role)->first();

        if (empty($employeeResultSearch) && empty($roleResultSearch)) {
            $output->writeln('<error>Search failed</error>');
            exit();
        }

        $employeeRole = EmployeeRole::where('employee_id', $employeeResultSearch->id)->where('role_id', $roleResultSearch->id)->first();

        if ($employeeRole) {
            $output->writeln('true');
        } else {
            $output->writeln('false');
        }

        return 0;
    }
}
