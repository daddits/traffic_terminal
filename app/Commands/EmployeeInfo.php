<?php

namespace Commands;
require_once 'config.php';

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Models\Employee;

/**
 * Class EmployeeInfo
 */
class EmployeeInfo extends Command
{
    /**
     * Console command for get info about employee's roles(configure)
     */
    protected function configure(): void
    {
        $this->setName('employee:info')
            ->setDescription('Get employee info')
            ->setHelp('Get employee info')
            ->addArgument('title', InputArgument::REQUIRED, 'title of employee');
    }

    /**
     * Console command for get info about employee's roles (execute)
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $title = $input->getArgument('title');
        /**@var Employee* */
        $searchResult = Employee::where('name', 'like', '%' . $title . '%')->with('roles')->get();

        if ($searchResult->count() === 0) {
            $output->writeln('<error>Employee not found</error>');
            exit();
        }

        foreach ($searchResult as $employee) {
            $roles = $employee->roles;
            foreach ($roles as $role) {
                $output->writeln($role->title);
            }

        }
        return 0;
    }
}