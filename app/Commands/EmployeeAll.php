<?php
namespace Commands;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Models\Employee;
/**
 * Class EmployeeAll
 */
class EmployeeAll extends Command
{
    /**
     * Console command for get all employees (configure)
     */
    protected function configure()
    {
        $this->setName('emlpoyee:all')
            ->setDescription('Get all employees')
            ->setHelp('Get all employees');
    }

    /**
     * Console command for get all employees (execute)
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $employees = Employee::all();
        $output->writeLn('id:name');
        foreach($employees as $employee){
            $output->writeln($employee->id.'|'.$employee->name);
        }
        return 0;
    }
}
