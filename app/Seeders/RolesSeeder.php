<?php

namespace Seeders;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Models\Role;

/**
 * Class RolesSeeder
 */
class RolesSeeder extends Command
{
    public const WRITE_CODE_ROLE       = 'write code';
    public const TEST_CODE_ROLE        = 'test code';
    public const COMMUNICATE_CODE_ROLE = 'communicate with the manager';
    public const DRAW_CODE_ROLE        = 'draw';
    public const SET_TASK_CODE_ROLE    = 'set_task';

    protected function configure(): void
    {
        $this->setName('seed:roles')
            ->setDescription('Seed roles table')
            ->setHelp('Seed roles table');
    }

    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        if ($this->createWriteCodeRole() && $this->createTestCodeRole() && $this->createCommunicateRole() && $this->createDrawRole() && $this->createSetTaskRole()) {
            $output->writeLn('<info>Roles seeded successfuly</info>');
            return 0;
        }
        $output->writeLn('<error>Error occurred while seed roles</error>');
    }

    private function createWriteCodeRole(): bool
    {
        $developer = new Role();
        $developer->title = self::WRITE_CODE_ROLE;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createTestCodeRole(): bool
    {
        $developer = new Role();
        $developer->title = self::TEST_CODE_ROLE;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createCommunicateRole(): bool
    {
        $developer = new Role();
        $developer->title = self::COMMUNICATE_CODE_ROLE;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createDrawRole(): bool
    {
        $developer = new Role();
        $developer->title = self::DRAW_CODE_ROLE;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createSetTaskRole(): bool
    {
        $developer = new Role();
        $developer->title = self::SET_TASK_CODE_ROLE;
        if ($developer->save()) {
            return true;
        }
        return false;
    }
}