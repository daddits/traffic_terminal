<?php

namespace Seeders;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Models\Employee;

/**
 * Class EmployeeSeeder
 */
class EmployeesSeeder extends Command
{
    public const EMPLOYEE_DEVELOPER = 'developer';
    public const EMPLOYEE_DESIGNER  = 'designer';
    public const EMPLOYEE_TESTER    = 'tester';
    public const EMPLOYEE_MANAGER   = 'manager';

    protected function configure(): void
    {
        $this->setName('seed:employee')
            ->setDescription('Create new employee')
            ->setHelp('Create new  fake employee');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        if ($this->createDesigner() && $this->createDeveloper() && $this->createTester() && $this->createManager()) {
            $output->writeLn('<info>Employee\'s created successfuly</info>');
            return 0;
        }
        $output->writeLn('<error>Error occurred while creating employees</error>');

    }

    private function createDeveloper(): bool
    {
        $developer = new Employee();
        $developer->name = self::EMPLOYEE_DEVELOPER;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createDesigner(): bool
    {
        $developer = new Employee();
        $developer->name = self::EMPLOYEE_DESIGNER;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createTester(): bool
    {
        $developer = new Employee();
        $developer->name = self::EMPLOYEE_TESTER;
        if ($developer->save()) {
            return true;
        }
        return false;
    }

    private function createManager(): bool
    {
        $developer = new Employee();
        $developer->name = self::EMPLOYEE_MANAGER;
        if ($developer->save()) {
            return true;
        }
        return false;
    }
}