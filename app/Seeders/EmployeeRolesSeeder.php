<?php

namespace Seeders;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Models\EmployeeRole;

/**
 * Class EmployeeRolesSeeder
 */
class EmployeeRolesSeeder extends Command
{
    protected function configure(): void
    {
        $this->setName('seed:employee-role')
            ->setDescription('Create new employee role')
            ->setHelp('Create new  fake employee role');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null
     */
    protected function execute(InputInterface $input, OutputInterface $output): ?int
    {
        $this->getRolesForDeveloper();
        $this->getRolesForDesigner();
        $this->getRolesForTester();
        $this->getRolesForManager();

        $output->writeLn('<info>Employee\'s created successfuly</info>');
        return 0;
    }

    /**
     * seeder for developer
     */
    private function getRolesForDeveloper(): void
    {
        $data = [];
        $data[] = ['employee_id' => 1, 'role_id' => 4];
        $data[] = ['employee_id' => 1, 'role_id' => 5];

        $this->seedData($data);
    }

    /**
     * seeder for designer
     */
    private function getRolesForDesigner(): void
    {
        $data = [];
        $data[] = ['employee_id' => 2, 'role_id' => 1];
        $data[] = ['employee_id' => 2, 'role_id' => 2];
        $data[] = ['employee_id' => 2, 'role_id' => 5];

        $this->seedData($data);
    }

    /**
     * seeder for tester
     */
    private function getRolesForTester(): void
    {
        $data = [];
        $data[] = ['employee_id' => 3, 'role_id' => 1];
        $data[] = ['employee_id' => 3, 'role_id' => 4];

        $this->seedData($data);
    }

    /**
     * seeder for manager
     */
    private function getRolesForManager(): void
    {
        $data = [];
        $data[] = ['employee_id' => 4, 'role_id' => 1];
        $data[] = ['employee_id' => 4, 'role_id' => 2];
        $data[] = ['employee_id' => 4, 'role_id' => 5];

        $this->seedData($data);
    }

    /**
     * @param array $data
     */
    private function seedData(array $data):void
    {
        foreach ($data as $one) {
            $employeeRole = new EmployeeRole();
            $employeeRole->employee_id = $one['employee_id'];
            $employeeRole->role_id = $one['role_id'];
            $employeeRole->save();
        }
    }
}