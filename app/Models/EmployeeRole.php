<?php

namespace Models;

use Illuminate\Database\Eloquent\Model as Eloquent;

/**
 * Class EmployeeRole
 */
class EmployeeRole extends Eloquent
{
    protected $table = 'employee_role';
}